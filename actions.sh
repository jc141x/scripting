#!/usr/bin/env bash

# Ensure script is not run as root
[ "$EUID" -eq 0 ] && { echo "this script should not be run as root"; exit 1; }

# define runtime dependencies and check their availability
declare -A DEPENDENCIES=(
    ['dwarfs']='dwarfs'
    ['bwrap']='bubblewrap'
    ['fuse-overlayfs']='fuse-overlayfs'
)

for dep_bin in "${!DEPENDENCIES[@]}"; do
    if ! command -v "$dep_bin" &> /dev/null; then
        echo "error: ${DEPENDENCIES[$dep_bin]} is not installed or not executable"
        exit 1
    fi
done

GAME_ROOT="$PWD/files/game-root"

dwarfs-mount() {
	dwarfs-unmount &> /dev/null

	HWRAMTOTAL="$(grep MemTotal /proc/meminfo | awk '{print $2}')"
	CACHEONRAM=$((HWRAMTOTAL * 25 / 100))

	CORUID="$(id -u $USER)"
	CORGID="$(id -g $USER)"

	[ -d "$GAME_ROOT" ] && { [ "$(ls -A "$GAME_ROOT")" ] && echo "game is mounted or extracted." && return 0; }

    mkdir -p "$PWD/files/.game-root-mnt" "$PWD/files/overlay-storage" "$PWD/files/.game-root-work" "$GAME_ROOT" || {
        return 1
    }

    dwarfs "$PWD/files/game-root.dwarfs" "$PWD/files/.game-root-mnt" \
        -o tidy_strategy=time -o tidy_interval=15m -o tidy_max_age=30m \
        -o cachesize="${CACHEONRAM}k" -o clone_fd -o cache_image && \
        fuse-overlayfs -o squash_to_uid="$CORUID" \
                        -o squash_to_gid="$CORGID" \
                        -o lowerdir="$PWD/files/.game-root-mnt",upperdir="$PWD/files/overlay-storage",workdir="$PWD/files/.game-root-work" \
                        "$GAME_ROOT" && \
	echo "game mounted successfully. extraction not required."

}

dwarfs-unmount() {
    fuser -k "$PWD/files/.game-root-mnt" 2>/dev/null

    local UMOUNT_DIRS=("$GAME_ROOT" "$PWD/files/.game-root-mnt")
    for dir in "${UMOUNT_DIRS[@]}"; do
        fusermount3 -u -z "$dir" 2>/dev/null
    done

    echo "game unmounted."

    rm -rf "$PWD/files/.game-root-mnt" "$PWD/files/.game-root-work"
    [ -d "$GAME_ROOT" ] && [ -z "$(ls -A "$GAME_ROOT")" ] && rm -rf "$GAME_ROOT"
}

dwarfs-extract() {
    if [ -d "$GAME_ROOT" ] && [ "$(ls -A "$GAME_ROOT")" ]; then
        echo "game is already mounted or extracted"
        return 0
    fi

    mkdir -p "$GAME_ROOT" || {
        return 1
    }

    dwarfsextract --stdout-progress -i "$PWD/files/game-root.dwarfs" -o "$GAME_ROOT" || {
        echo "error: failed to extract game files"
        return 1
    }
}

dwarfs-compress() {
    if [ -f "$PWD/files/game-root.dwarfs" ]; then
        return 0
    fi

    mkdwarfs -l7 -B26 -S26 \
        --no-history \
        --order=nilsimsa \
        --set-owner=1000 \
        --set-group=1000 \
        --set-time=now \
        --chmod=Fa+rw,Da+rwx \
        -i "$GAME_ROOT" \
        -o "$PWD/files/game-root.dwarfs" && \
        tree -a -s files/game-root > files/dwarfs-tree
}

dwarfs-check_integrity() { dwarfsck --check-integrity -i "$PWD/files/game-root.dwarfs"; }

jc141-cleanup() { cd "$OLDPWD" && dwarfs-unmount; }

wine-initiate_prefix() {
    wineboot -i
    find "$WINEPREFIX/drive_c/users/$USER" -maxdepth 1 -type l -exec test -d {} \; -exec rm {} \; -exec mkdir {} \;
    wineserver -w
}

wine-setup_external_vulkan() {
    if tar -xvf "$PWD/files/vulkan.tar.xz" -C "$PWD/files" && bash "$PWD/files/vulkan/setup-vulkan.sh"; then
        echo "vulkan installed" > "$WINEPREFIX/vulkan.log"
        rm -rf "$PWD/files/vulkan"
    else
        echo "error: failed to set up vulkan"
    fi
}

jc141-write_config() {
	cat <<- 'EOF' >> "$1"
	# automatically unmounts game files after the process ends
	UNMOUNT=1

	# extract game files instead of mounting the dwarfs archive on launch
	EXTRACT=0

	# display terminal output
	TERMINAL_OUTPUT=1

	# wine executable path
	SYSWINE="$(command -v wine)"


	# bubblewrap

	# force games into isolation sandbox
	ISOLATE=1

	# block network access to the game. Does not work if ISOLATE=0.
	BLOCK_NET=1

	# sandbox directory path for isolation. (stores wine prefix, game saves, etc..)
	# only used when ISOLATE=1 for native games. the path is used regardless for the wine prefix
	JC_DIRECTORY="$HOME/Games/jc141"


	# gamescope
	GAMESCOPE=1
	GAMESCOPE_FULLSCREEN=1
	GAMESCOPE_BORDERLESS=0

	# output resolution
	GAMESCOPE_SCREEN_WIDTH=
	GAMESCOPE_SCREEN_HEIGHT=

	# game resolution
	GAMESCOPE_GAME_WIDTH=
	GAMESCOPE_GAME_HEIGHT=

	# additional flags (run "gamescope --help" for options)
	ADDITIONAL_FLAGS=""
	EOF
}

jc141-generate_global_defaults() {
	cat <<- 'EOF' > "$HOME/.jc141rc"
	# this config is used by jc141 start scripts to specify default settings.
	# these settings are applied globally, unless overridden by the game-specific config located beside the launch scripts

	EOF

	jc141-write_config "$HOME/.jc141rc"

}

jc141-generate_local_overrides() {

	cat <<- 'EOF' > "$PWD/script_default_settings"
	# this file is used by jc141 start scripts to specify game-specific settings
	# these settings are applied only to this game, and override the global
	# configuration specified in "~/.jc141rc"

	# by default, all settings in this file are commented out (disabled) with a #

	EOF

	jc141-write_config "$PWD/script_default_settings"

	# comment out config keys by default
	sed -i -e 's/^\([^#].*\)/#\1/g' "$PWD/script_default_settings"

}

bwrap-run_in_sandbox() {
	[ -z "${XDG_RUNTIME_DIR}" ] && export XDG_RUNTIME_DIR="/run/user/${EUID}"
	BWRAP_FLAGS=(--ro-bind / / --dev-bind-try /dev /dev --bind-try /tmp /tmp)

	[ "$ISOLATION_TYPE" = 'wine' ] && BWRAP_FLAGS+=( --bind "$WINEPREFIX" "$WINEPREFIX" )
	[ "$ISOLATION_TYPE" = 'native' ] && BWRAP_FLAGS+=( --bind-try "$JC_DIRECTORY/native-docs" ~/ ) && [ ! -e "$JC_DIRECTORY/native-docs/.Xauthority" ] && ln "$XAUTHORITY" "$JC_DIRECTORY/native-docs" && XAUTHORITY="$HOME/.Xauthority"

	[ $BLOCK_NET = 1 ] && BWRAP_FLAGS+=( --unshare-net )

	# current dir as last setting
	BWRAP_FLAGS+=( --bind "$PWD" "$PWD" )

	bwrap "${BWRAP_FLAGS[@]}" "$@"
}

gamescope-run_embedded() {
    GAMESCOPE_BIN="$(command -v gamescope)"
    [ $GAMESCOPE_FULLSCREEN -eq 1 ] && GAMESCOPE_ARGS+=(-f)
    [ $GAMESCOPE_BORDERLESS -eq 1 ] && GAMESCOPE_ARGS+=(-b)
    [ -n "$GAMESCOPE_SCREEN_WIDTH" ] && GAMESCOPE_ARGS+=(-W "$GAMESCOPE_SCREEN_WIDTH")
    [ -n "$GAMESCOPE_SCREEN_HEIGHT" ] && GAMESCOPE_ARGS+=(-H "$GAMESCOPE_SCREEN_HEIGHT")
    [ -n "$GAMESCOPE_GAME_WIDTH" ] && GAMESCOPE_ARGS+=(-w "$GAMESCOPE_GAME_WIDTH")
    [ -n "$GAMESCOPE_GAME_HEIGHT" ] && GAMESCOPE_ARGS+=(-h "$GAMESCOPE_GAME_HEIGHT")
    GAMESCOPE_ARGS+=($ADDITIONAL_FLAGS)

    "$GAMESCOPE_BIN" "${GAMESCOPE_ARGS[@]}" -- "$@"
}

# help function
help() {
    cat << 'EOF'

Usage: actions.sh [SUBCOMMAND]

  This script provides the following subcommands:

    dwarfs-mount                   Mounts the DwarFS archive, with an overlay for storing changes.
    dwarfs-unmount                 Unmounts the DwarFS archive and cleans up overlay directories.
    dwarfs-extract                 Extracts the entire DwarFS archive into a normal directory.
    dwarfs-compress                Compresses the existing game-root folder back into a DwarFS archive.
    dwarfs-check_integrity         Checks integrity of the .dwarfs file using dwarfsck.

  USAGE EXAMPLE:                  bash actions.sh dwarfs-extract

  NOTE:
    - If you want to override default settings, edit the "script_default_settings" file
      placed alongside this script (or the global ~/.jc141rc).
    - Need further help? Join us on Matrix at:
        https://matrix.to/#/#rumpowered:matrix.org

EOF
}

# generate config files if not present
[ ! -f "$HOME/.jc141rc" ] && jc141-generate_global_defaults
[ ! -f "$PWD/script_default_settings" ] && jc141-generate_local_overrides

# load config files
source "$HOME/.jc141rc"
source "$PWD/script_default_settings"

# run
(return 0 2> /dev/null) || {
	if type "$1" &> /dev/null; then
		"$1" "${@:2}"
	else
		exit
	fi
}